var gulp        = require('gulp');
var browserSync = require('browser-sync');

// start server
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});

// browser reload
gulp.task('default', ['browser-sync'], function () {
    gulp.watch("css/*.css", browserSync.reload);
    gulp.watch("*.html", browserSync.reload);
});